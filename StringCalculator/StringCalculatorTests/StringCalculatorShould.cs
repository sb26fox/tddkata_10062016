﻿using NUnit.Framework;
using StringCal;

namespace StringCalculatorTests
{
    [TestFixture]
    public class StringCalculatorShould
    {
        private StringCalculator CreateCalculator()
        {
            return new StringCalculator();
        }

        [Test]
        public void Return_1_IfInput_Is_1()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("1");
            Assert.AreEqual(1, result);
        }

        [Test]
        public void ReturnNumber_IfInput_IsDidgitalNumber()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("10");
            Assert.AreEqual(10, result);
        }

        [Test]
        public void ReturnNunmber_IfInput_IsNumber()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("2");
            Assert.AreEqual(2, result);
        }

        [Test]
        public void ReturnSummOfNumber_IfInput_IsTwoNumbers()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("1,2");
            Assert.AreEqual(1 + 2, result);
        }

        [Test]
        public void ReturnSumOfNumbers_IfInput_HasCustomDelimiter()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("//[-]\n1-2-3");
            Assert.AreEqual(6, result);
        }

        [Test]
        public void ReturnSumOfNumbers_IfInput_HasCustomDelimiterLongerThanOneChar()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("//[*-*]\n1*-*2*-*3");
            Assert.AreEqual(6, result);
        }

        [Test]
        public void ReturnSumOfNumbers_IfInput_HasMultipleDelimiters()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("//[-][*]\n1-2*3");
            Assert.AreEqual(6, result);
        }

        [Test]
        public void ReturnSumOfNumbers_IfInput_HasMultipleDelimitersLongerThanOneChar()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("//[--][**]\n1--2**3");
            Assert.AreEqual(6, result);
        }

        [Test]
        public void ReturnSumOfNumbers_IfInput_HasNewLineDelimiter()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("1\n2,3");
            Assert.AreEqual(1 + 2 + 3, result);
        }

        [Test]
        public void ReturnSumOfSeveralNumbers_IfInput_IsSeveralNumbers()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("1,2,3");
            Assert.AreEqual(1 + 2 + 3, result);
        }

        [Test]
        public void ReturnZero_IfInput_HasWrongDelimiter()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add("1,\n2,3");
            Assert.AreEqual(0, result);
        }

        [Test]
        public void ReturnZero_IfInput_IsZero()
        {
            var calculator = CreateCalculator();
            var result = calculator.Add(string.Empty);
            Assert.AreEqual(0, result);
        }

        [Test]
        public void ThrowsNegativeNumberException_IfInput_HasNegativeNumbers()
        {
            var calculator = CreateCalculator();
            Assert.Throws<NegativeNumberException>(() => calculator.Add("-1,2,3"));
        }
    }
}
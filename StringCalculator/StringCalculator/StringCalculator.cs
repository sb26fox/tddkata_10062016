﻿using System;
using System.Collections.Generic;

namespace StringCal
{
    public class StringCalculator
    {
        private readonly int defaultValue = 0;

        public int Add(string input)
        {
            if (IsEmpty(input) || HasWrongDelimiter(input))
            {
                return defaultValue;
            }

            if (HasCustomDelimiter(input))
            {
                var delimiter = GetDelimiter(input);
                var destNumbers = GetNumbers(input);
                var numbers = destNumbers.Split(delimiter, StringSplitOptions.None);

                return Count(numbers);
            }

            if (HasStandartDelimiter(input))
            {
                var numbers = SplitByDefaultDilimter(input);

                return Count(numbers);
            }


            return Parse(input);
        }

        private int Count(string[] numbers)
        {
            var negativeNumbers = string.Empty;
            var count = 0;

            foreach (var number in numbers)
            {
                if (Parse(number) < 0)
                {
                    string.Concat(negativeNumbers, " ", number);
                }
            }

            foreach (var number in numbers)
            {
                if (Parse(number) < 0)
                {
                    throw new NegativeNumberException("Содержатся отрицательные числа " + negativeNumbers);
                }
                count += Parse(number);
            }

            return count;
        }

        private string[] GetDelimiter(string input)
        {
            var delimiters = new List<string>();
            var index = input.IndexOf("\n");

            input.Remove(index);
            var del = input.Split('[', ']');

            for (var i = 1; i < del.Length - 1; i++)
            {
                delimiters.Add(del[i]);
            }

            return delimiters.ToArray();
        }

        private string GetNumbers(string input)
        {
            return input.Substring(input.IndexOf("\n"));
        }

        private bool HasCustomDelimiter(string input)
        {
            return input.StartsWith("//");
        }

        private string[] SplitByDefaultDilimter(string input)
        {
            return input.Split(',', '\n');
        }

        private bool HasWrongDelimiter(string input)
        {
            return input.Contains(",\n");
        }

        private bool HasStandartDelimiter(string input)
        {
            return input.Contains(",") || input.Contains("\n");
        }

        private int Parse(string input)
        {
            return int.Parse(input);
        }

        private bool IsEmpty(string input)
        {
            return string.IsNullOrEmpty(input);
        }
    }
}